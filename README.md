﻿ -- INSTRUÇÕES DE INSTALAÇÃO -- 

## app-students

 ##


BACKEND

1. Crie um arquivo local com o nome '.env', baseado no arquivo '.env.example' e preencha as variáveis de ambiente.
A variável de ambiente SERVER_IP deve ser preenchida com o IP local da máquina.



2.Na pasta 'backend', execute o comando 'yarn install' ou 'npm install' para baixar as dependências do projeto.



3. Para subir o sevidor execute 'yarn dev' ou 'npm run dev'.




## APP

1 ##
. Instale a ferramenta 'Expo' com o comando: npm install --global expo-cli

2. Na pasta 'app', execute o comando 'yarn install'
ou 'npm install' para baixar as dependências do projeto.



2. No arquivo 'src/services/api.js', altere o valor da constante 'SERVER_IP' para o IP local da máquina. 



3. Para executar o aplicativo rode um 'expo start' no console.



4. Baixe o aplicativo 'Expo Go' na loja de aplicativos, depois de instalado vá na opção 'Scan QR Code' e leia o código no console ou na guia do navegador que se abrirá.
